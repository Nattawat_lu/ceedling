#ifndef _TEMPSENSOR_H
#define _TEMPSENSOR_H
# include <stdint.h>

//#define i2c_readRegister ASTB_i2c_readRegister

float tempSensor_getTemperature(void);

#endif // _TEMPSENSOR_H
